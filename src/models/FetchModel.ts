import config from './Config';
import { Club } from '@/abstracts/types/interfaces';

class FetchModel {
  public apiUrl: string;

  constructor(instanceName: string) {
    this.apiUrl = `${config.apiBaseUrl}${instanceName}s/`;
  }

  async sendRequest(method: string, data: Club | null | number = null)
    : Promise<void | null | Response> {
    try {
      const params = data || '';
      switch (method) {
        case 'GET':
        case 'DELETE':
          return await fetch(`${this.apiUrl}${params}`, {
            method,
          });
        case 'POST':
        case 'PUT':
          return await fetch(`${this.apiUrl}`, {
            method,
            headers: {
              'content-type': 'application/json',
            },
            body: JSON.stringify(data),
          });
        default: return null;
      }
    } catch (err) {
      return Promise.reject(err);
    }
  }
}

export default FetchModel;
