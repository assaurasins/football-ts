import 'regenerator-runtime/runtime';
import FetchModel from './FetchModel';
import { Club } from '@/abstracts/types/interfaces';

class ApiModel {
  public apiModel: FetchModel;

  constructor() {
    this.apiModel = new FetchModel('club');
  }

  async initInstances(): Promise<Response| void | null> {
    try {
      // return this.apiModel.sendRequest('GET').then((res) => res.json());
      const response = await this.apiModel.sendRequest('GET');

      if (response) {
        return response.json();
      }
      return response;
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async initInstanceById(id: number): Promise<Response | null | void> {
    try {
      const response = await this.apiModel.sendRequest('GET', id);

      if (response) {
        return response.json();
      }
      return response;
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async deleteInstance(id: number): Promise<void | Response | null> {
    try {
      return this.apiModel.sendRequest('DELETE', id);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async addInstance(formData: Club): Promise<void | Response | null> {
    try {
      return this.apiModel.sendRequest('POST', formData);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async updateInstance(formData: Club): Promise<void | Response | null> {
    try {
      return this.apiModel.sendRequest('PUT', formData);
    } catch (err) {
      return Promise.reject(err);
    }
  }
}

export default ApiModel;
