import { ConfigType } from '@/abstracts/types/interfaces';

const config: ConfigType = {
  apiBaseUrl: process.env.VUE_APP_API_BASE_URL,
  baseUrl: process.env.VUE_APP_BASE_URL,
};

export default config;
