import { Commit } from 'vuex';

export interface Club {
  id?: number | null,
  name: string,
  country: string,
}

export interface ConfigType {
  [key: string]: string,
}

export interface RoutesType {
  [key: string]: string,
}

export interface CommitType {
  commit: Commit;
}
