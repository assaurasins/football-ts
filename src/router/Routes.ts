import { RoutesType } from '@/abstracts/types/interfaces';

const routesMap: RoutesType = {
  clubsPathName: '/clubs',
};

export default routesMap;
