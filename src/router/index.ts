import Vue from 'vue';
import VueRouter, { Route, RouteConfig } from 'vue-router';
import Home from '../views/HomeView.vue';
import config from '../models/Config';
import routesMap from './Routes';

Vue.use(VueRouter);

const RADIX = 10;

function dynamicPropsFn(route: Route): {id: number} {
  return {
    id: parseInt(route.params.id, RADIX),
  };
}

const routes: Array<RouteConfig> = [
  {
    path: '/',
    component: Home,
  },
  {
    path: routesMap.clubsPathName,
    name: 'ClubsView',
    component: () => import('../views/ClubsView.vue'),
  },
  {
    path: `${routesMap.clubsPathName}/:id`,
    name: 'ClubView',
    component: () => import('../views/ClubView.vue'),
    props: dynamicPropsFn,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: config.baseUrl,
  routes,
});

export default router;
