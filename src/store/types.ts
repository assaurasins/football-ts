import { Club } from '@/abstracts/types/interfaces';

export interface ClubsState {
  clubs: Club[],
  openedClub: Club,
}
