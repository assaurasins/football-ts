import { ActionTree } from 'vuex';
import { ClubsState } from '@/store/types';
import ApiModel from '../../../models/ApiModel';
import { CommitType, Club } from '@/abstracts/types/interfaces';

const apiModel = new ApiModel();

const actions: ActionTree<ClubsState, ClubsState> = {
  async addClub({ commit }: CommitType, club: Club): Promise<void> {
    try {
      await apiModel.addInstance(club);
      commit('addClub', club);
    } catch (err) {
      console.log('can not add club');
    }
  },

  async getClubs({ commit }: CommitType): Promise<void> {
    try {
      const clubs = await apiModel.initInstances();
      commit('setClubs', clubs);
    } catch {
      console.log('Can not get any clubs');
    }
  },

  async initClubById({ commit }: CommitType, id: number): Promise<void> {
    try {
      const club = await apiModel.initInstanceById(id);
      commit('setOpenedClub', club);
    } catch {
      console.log('Can not get club');
    }
  },

  async deleteClub({ commit }: CommitType, id: number): Promise<void> {
    try {
      await apiModel.deleteInstance(id);
      commit('deleteClub', id);
    } catch {
      console.log('Can not delete club');
    }
  },

  async updateClub({ commit }: CommitType, formData: Club): Promise<void> {
    try {
      const club = await apiModel.updateInstance(formData);
      commit('setClub', club);
    } catch (err) {
      console.log('Can not update club');
    }
  },
};

export default actions;
