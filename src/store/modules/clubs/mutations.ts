import { MutationTree } from 'vuex';
import { ClubsState } from '@/store/types';
import { Club } from '@/abstracts/types/interfaces';

const mutations: MutationTree<ClubsState> = {
  addClub(state, club: Club) {
    state.clubs.push(club);
  },

  setClubs(state, clubs: Club[]) {
    state.clubs = clubs;
  },

  setOpenedClub(state, club: Club) {
    state.openedClub = club;
  },

  setClub(state, club: Club) {
    const index = state.clubs.findIndex((p) => p.id === club.id);
    state.clubs[index] = club;
  },

  deleteClub(state, id: number) {
    const index = state.clubs.findIndex((p) => p.id === id);
    state.clubs.splice(index, 1);
  },
};

export default mutations;
