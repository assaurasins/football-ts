import { GetterTree } from 'vuex';
import { Club } from '@/abstracts/types/interfaces';

import { ClubsState } from '@/store/types';

const getters: GetterTree<ClubsState, ClubsState> = {
  getClubs(state): Club[] {
    return state.clubs;
  },

  getOpenedClub(state): Club {
    return state.openedClub;
  },
};

export default getters;
