import { ClubsState } from '@/store/types';

const state: ClubsState = {
  clubs: [],
  openedClub: {
    id: null,
    name: '',
    country: '',
  },
};

export default state;
